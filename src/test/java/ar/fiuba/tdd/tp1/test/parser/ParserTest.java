package ar.fiuba.tdd.tp1.test.parser;

import ar.fiuba.tdd.tp1.model.Excel;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;
import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.parser.FormulaCellParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    private static final float DELTA = 0.01f;

    @Test
    public void parserShouldThrowNumberOperandIfNumberIsPassed() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        Value myOperand = myParser.parse("", "= 45");

        assertEquals(ExpressionDefault.class, myOperand.getClass());
    }

    @Test
    public void parserNumberOperandShouldBeTheIntegerPassed() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        Value myOperand = myParser.parse("", "= 45");

        assertEquals(45f, myOperand.getValue().getNumerical(), DELTA);
    }


    @Test(expected = IllegalArgumentException.class)
    public void parserShouldThrowIllegalArgumentExceptionIfWeDontGiveAFormulaOrNumber() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        myParser.parse("", "= ?");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parserShouldThrowIllegalArgumentExceptionIfWeGiveParenthesisClosed() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        myParser.parse("", "= 1 + 4 + ) - 5");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parserShouldThrowIllegalArgumentExceptionIfWeGiveTwoSpaces() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        myParser.parse("", "=  ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parserShouldThrowIllegalArgumentExceptionIfWeGiveTwoValidOperands() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        myParser.parse("", "= 3 ++ 2");
    }

    @Test
    public void parserShouldReturnAValidSumFormula() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 2 + 5");
        assertEquals(7f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void parserShouldReturnAValidSubFormula() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 7 - 3");
        assertEquals(4f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void parserShouldReturnAMultiFormula() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 2 + 5 - 4");
        assertEquals(3f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void parserShouldReturnAMultiGigantFormula() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 5 - 45 + 50 - 8 + 20 - 30 + 10 - 50 + 80 - 30");
        assertEquals(2f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void parserCellShouldBeAddedOnFormula() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.createNewWorkSheetNamed("Prueba", "H4");
        excel.setCellValue("Prueba", "H1", "B2", "= 20");
        excel.setCellValue("Prueba", "H4", "A1", "= 10 + !H1.B2");
        assertEquals(30f, excel.getCellValueAsDouble("Prueba", "H4", "A1"), DELTA);
    }

    @Test
    public void parserWithTwoCells() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.createNewWorkSheetNamed("Prueba", "H4");
        excel.createNewWorkSheetNamed("Prueba", "H3");
        excel.setCellValue("Prueba", "H1", "B2", "= 20");
        excel.setCellValue("Prueba", "H4", "A1", "= 10");
        excel.setCellValue("Prueba", "H3", "A1", "= !H1.B2 + !H4.A1");

        assertEquals(30f, excel.getCellValueAsDouble("Prueba", "H3", "A1"), DELTA);
    }

    @Test
    public void parenthesesInParser() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 10 - (2 + 5)");
        assertEquals(3f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void doubleParenthesesInParser() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= 15 - (15 - (9 - 2))");
        assertEquals(7f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void maxOperationInParser() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= MAX(1,8)");
        assertEquals(8f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void minOperationInParser() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= MIN(1,8)");
        assertEquals(1f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void compositeMinWithOthers() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= MIN(1 + 4,8)");
        assertEquals(5f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void minWithCells() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.setCellValue("Prueba", "H1", "A1", "= 20");
        excel.setCellValue("Prueba", "H1", "A2", "= 10");
        excel.setCellValue("Prueba", "H1", "A3", "= MIN(!H1.A2,!H1.A1)");

        assertEquals(10f, excel.getCellValueAsDouble("Prueba", "H1", "A3"), DELTA);
    }

    @Test
    public void maxWithRange() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.setCellValue("Prueba", "H1", "A1", "= 20");
        excel.setCellValue("Prueba", "H1", "A2", "= 5");
        excel.setCellValue("Prueba", "H1", "A3", "= 15");
        excel.setCellValue("Prueba", "H1", "A4", "= 25");
        excel.setCellValue("Prueba", "H1", "B1", "= 33");
        excel.setCellValue("Prueba", "H1", "B2", "= 41");
        excel.setCellValue("Prueba", "H1", "B3", "= 5");
        excel.setCellValue("Prueba", "H1", "B4", "= 4");
        excel.setCellValue("Prueba", "H1", "C1", "= 12");
        excel.setCellValue("Prueba", "H1", "C2", "= 45");
        excel.setCellValue("Prueba", "H1", "C3", "= 33");
        excel.setCellValue("Prueba", "H1", "C4", "= 65");
        excel.setCellValue("Prueba", "H1", "E1", "= MAX(A1:C4)");

        assertEquals(65f, excel.getCellValueAsDouble("Prueba", "H1", "E1"), DELTA);
    }

    @Test
    public void minWithRange() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.setCellValue("Prueba", "H1", "A1", "= 20");
        excel.setCellValue("Prueba", "H1", "A2", "= 5");
        excel.setCellValue("Prueba", "H1", "A3", "= 15");
        excel.setCellValue("Prueba", "H1", "A4", "= 25");
        excel.setCellValue("Prueba", "H1", "B1", "= 33");
        excel.setCellValue("Prueba", "H1", "B2", "= 41");
        excel.setCellValue("Prueba", "H1", "B3", "= 5");
        excel.setCellValue("Prueba", "H1", "B4", "= 4");
        excel.setCellValue("Prueba", "H1", "C1", "= 12");
        excel.setCellValue("Prueba", "H1", "C2", "= 45");
        excel.setCellValue("Prueba", "H1", "C3", "= 33");
        excel.setCellValue("Prueba", "H1", "C4", "= 65");
        excel.setCellValue("Prueba", "H1", "E1", "= MIN(A1:C4)");

        assertEquals(4f, excel.getCellValueAsDouble("Prueba", "H1", "E1"), DELTA);
    }

    @Test
    public void minWithSameColRange() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.setCellValue("Prueba", "H1", "A1", "= 20");
        excel.setCellValue("Prueba", "H1", "A2", "= 5");
        excel.setCellValue("Prueba", "H1", "A3", "= 15");
        excel.setCellValue("Prueba", "H1", "A4", "= 25");
        excel.setCellValue("Prueba", "H1", "E1", "= MIN(A1:A4)");

        assertEquals(5f, excel.getCellValueAsDouble("Prueba", "H1", "E1"), DELTA);
    }

    @Test
    public void averageWithRange() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.setCellValue("Prueba", "H1", "A1", "= 20");
        excel.setCellValue("Prueba", "H1", "A2", "= 5");
        excel.setCellValue("Prueba", "H1", "A3", "= 15");
        excel.setCellValue("Prueba", "H1", "A4", "= 25");
        excel.setCellValue("Prueba", "H1", "B1", "= 33");
        excel.setCellValue("Prueba", "H1", "B2", "= 41");
        excel.setCellValue("Prueba", "H1", "B3", "= 5");
        excel.setCellValue("Prueba", "H1", "B4", "= 4");
        excel.setCellValue("Prueba", "H1", "C1", "= 12");
        excel.setCellValue("Prueba", "H1", "C2", "= 45");
        excel.setCellValue("Prueba", "H1", "C3", "= 33");
        excel.setCellValue("Prueba", "H1", "C4", "= 65");
        excel.setCellValue("Prueba", "H1", "E1", "= AVERAGE(A1:C4)");

        assertEquals(25.25f, excel.getCellValueAsDouble("Prueba", "H1", "E1"), DELTA);
    }

    @Test
    public void concactValueTest() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.getDocument("Prueba").getCell("H1", "A1").setExpression(new ExpressionDefault("Hola"));
        excel.getDocument("Prueba").getCell("H1", "B1").setExpression(new ExpressionDefault(" todo bien?"));

        excel.setCellValue("Prueba", "H1", "C1", "= CONCAT(A1,B1)");
        assertEquals("Hola todo bien?", excel.getDocument("Prueba").getCell("H1", "C1").getValue().getLiteral());
    }

    @Test
    public void concactValueRangeTest() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.getDocument("Prueba").getCell("H1", "A1").setExpression(new ExpressionDefault("Hola"));
        excel.getDocument("Prueba").getCell("H1", "B1").setExpression(new ExpressionDefault(" todo"));
        excel.getDocument("Prueba").getCell("H1", "C1").setExpression(new ExpressionDefault(" bien"));
        excel.getDocument("Prueba").getCell("H1", "D1").setExpression(new ExpressionDefault("?"));

        excel.setCellValue("Prueba", "H1", "E1", "= CONCAT(A1:D1)");
        assertEquals("Hola todo bien?", excel.getDocument("Prueba").getCell("H1", "E1").getValue().getLiteral());
    }

    @Test(expected = IllegalArgumentException.class)
    public void parserShouldThrowIllegalArgumentIfWeMixTheStringAndAritmeticalOperands() {
        FormulaCellParser myParser = new FormulaCellParser(null);
        myParser.parse("", "= 3 + CONCAT(A1,B3)");
    }

    @Test
    public void valueMidWith4Cells() {
        FormulaCellParser myParser = new FormulaCellParser(null);

        Value myOperand = myParser.parse("", "= MIN(7,10,5,4)");
        assertEquals(4f, myOperand.getValue().getNumerical(), DELTA);
    }

    @Test
    public void concactMidWith4Cells() {
        Excel excel = new Excel();
        excel.createNewWorkBookNamed("Prueba");
        excel.createNewWorkSheetNamed("Prueba", "H1");
        excel.getDocument("Prueba").getCell("H1", "A1").setExpression(new ExpressionDefault("Hola"));
        excel.getDocument("Prueba").getCell("H1", "B1").setExpression(new ExpressionDefault(" todo"));
        excel.getDocument("Prueba").getCell("H1", "C1").setExpression(new ExpressionDefault(" bien"));
        excel.getDocument("Prueba").getCell("H1", "D1").setExpression(new ExpressionDefault("?"));

        excel.setCellValue("Prueba", "H1", "E1", "= CONCAT(A1,B1,C1,D1)");
        assertEquals("Hola todo bien?", excel.getDocument("Prueba").getCell("H1", "E1").getValue().getLiteral());
    }
}