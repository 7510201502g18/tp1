package ar.fiuba.tdd.tp1.test.model;

import ar.fiuba.tdd.tp1.extern.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.model.Excel;
import ar.fiuba.tdd.tp1.model.SpreedSheet;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionCell;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionAverage;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionConcat;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionMax;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionMin;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ModelTest {

    private static final double DELTA = 0.0001;

    @Before
    public void setUp() {
    }

    @Test
    public void setExpressionNumber() {

        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault(1));

        assertEquals(1, sheet.get("A1").getValue().getNumerical(), DELTA);
    }

    @Test(expected = BadReferenceException.class)
    public void cycleRefrenceTest() {

        Excel excel = new Excel();
        excel.createNewWorkBookNamed("chelo");

        excel.setCellValue("chelo", "default", "A1", "= 5");
        excel.setCellValue("chelo", "default", "A2", "= 1 + A1");
        excel.setCellValue("chelo", "default", "A3", "= 1 + A2");
        excel.setCellValue("chelo", "default", "A4", "= 1 + A3");
        excel.setCellValue("chelo", "default", "A1", "= A4 + 2");
    }




    @Test
    public void setExpressionString() {

        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault("Hola"));

        assertEquals("Hola", sheet.get("A1").getValue().getLiteral());
    }

    @Test
    public void concatStrings() {
        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault("Hola"));
        sheet.get("B1").setExpression(new ExpressionDefault(" como"));
        sheet.get("C1").setExpression(new ExpressionDefault(" andas?"));
        sheet.get("A3").setExpression(new ExpressionConcat(sheet.get("A1"), sheet.get("B1"), sheet.get("C1")));
        assertEquals("Hola como andas?", sheet.get("A3").getValue().getAsString());
        sheet.get("C1").setExpression(new ExpressionDefault(" te va?"));
        assertEquals("Hola como te va?", sheet.get("A3").getValue().getAsString());
        sheet.get("D1").setExpression(new ExpressionDefault("de "));
        sheet.get("E1").setExpression(new ExpressionDefault("10"));
        sheet.get("A3").setExpression(new ExpressionConcat(sheet.get("D1"), sheet.get("E1")));
        assertEquals("de 10", sheet.get("A3").getValue().getAsString());
    }

    @Test
    public void average() {
        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault(2));
        sheet.get("B1").setExpression(new ExpressionCell(sheet.get("A1")));
        sheet.get("C1").setExpression(new ExpressionDefault(3));
        sheet.get("A3").setExpression(new ExpressionAverage(sheet.get("A1"), sheet.get("B1"), sheet.get("C1")));
        assertEquals((double) (2 + 2 + 3) / 3, sheet.get("A3").getValue().getNumerical(), DELTA);
    }

    @Test
    public void max() {
        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault(2));
        sheet.get("B1").setExpression(new ExpressionCell(sheet.get("A1")));
        sheet.get("C1").setExpression(new ExpressionDefault(3));
        sheet.get("A3").setExpression(new ExpressionMax(sheet.get("A1"), sheet.get("B1"), sheet.get("C1")));
        assertEquals(3, sheet.get("A3").getValue().getNumerical(), DELTA);
    }

    @Test
    public void min() {
        SpreedSheet sheet = new SpreedSheet("Default");
        sheet.get("A1").setExpression(new ExpressionDefault(2));
        sheet.get("B1").setExpression(new ExpressionCell(sheet.get("A1")));
        sheet.get("C1").setExpression(new ExpressionDefault(1));
        sheet.get("A3").setExpression(new ExpressionMin(sheet.get("A1"), sheet.get("B1"), sheet.get("C1")));
        assertEquals(1, sheet.get("A3").getValue().getNumerical(), DELTA);
    }

}
