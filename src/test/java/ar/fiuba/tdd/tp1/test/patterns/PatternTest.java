package ar.fiuba.tdd.tp1.test.patterns;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;


public class PatternTest {
    Pattern pattern;
    Matcher match;

    @Test
    public void datePattern() {
        pattern = Pattern.compile("^([0-9]{2})([^0-9;a-z;A-Z]{1})([0-9]{2})([^0-9]{1})([0-9]{4})$");
        match = pattern.matcher("18/12/1990");

        assertTrue(match.find());
    }

    @Test
    public void stringPattern() {
        pattern = Pattern.compile("[^0-9]*$");
        match = pattern.matcher("Hello World!");

        assertTrue(match.find());
    }

    @Test
    public void numberPattern() {
        pattern = Pattern.compile("(^-?[0-9]+$)|(^-?[0-9]+\\.[0-9]{2}$)");
        match = pattern.matcher("000");
        assertTrue(match.find());
        Matcher match = pattern.matcher("-2.99");
        assertTrue(match.find());
    }

    @Test
    public void moneyPattern() {
        pattern = Pattern.compile("^\\$[0-9]+$");
        match = pattern.matcher("$1313");

        assertTrue(match.find());
    }
}
