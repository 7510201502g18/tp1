package ar.fiuba.tdd.tp1.test.extern;

import ar.fiuba.tdd.tp1.extern.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.model.Excel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistenceTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new Excel();
    }

    @Test
    public void persistOneWorkBookAndRetrieveIt() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A1 + 1");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A2 + 1");
        testDriver.setCellValue("tecnicas", "default", "A4", "= A3 + 1");
        testDriver.setCellValue("tecnicas", "default", "A5", "= A4 + 1");
        testDriver.persistWorkBook("tecnicas", "proy1.json");
        testDriver.setCellValue("tecnicas", "default", "A1", "0");
        testDriver.setCellValue("tecnicas", "default", "A2", "0");
        testDriver.setCellValue("tecnicas", "default", "A3", "0");
        testDriver.reloadPersistedWorkBook("proy1.json");

        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }


}

