package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionAverage;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.Stack;

/**
 * Created by Demian on 17/10/2015.
 */
public class AverageRangeOperand extends RangeOperand {

    public AverageRangeOperand(Parser subParser) {
        super(subParser);
        preExp = "AVERAGE";
    }

    @Override
    protected void buildExpression(Value[] cellValueList, Stack<Expression> operandStack) {
        operandStack.push(new ExpressionAverage(cellValueList));
    }
}
