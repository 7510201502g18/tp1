package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.Document;

/**
 * Created by Demian on 19/10/2015.
 */
public class StringCellParser extends CellParser {
    Document excelFiuba;

    public StringCellParser(Document excelFiuba) {
        this.excelFiuba = excelFiuba;
        setOperators();
    }

    protected void setOperators() {
        baseOperands.add(new BaseOperandCellParser(excelFiuba));
        baseOperands.add(new ConcactValue(this));
        baseOperands.add(new ConcactRange(this));
    }
}
