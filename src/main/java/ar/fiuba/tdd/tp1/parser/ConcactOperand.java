package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.Stack;

/**
 * Created by Demian on 17/10/2015.
 */
public class ConcactOperand extends ParenthesesMatchParser {

    ConcactOperand(Parser subParser) {
        super(subParser);
        preExp = "CONCAT";
    }

    @Override
    protected boolean resolveInternalExpression(String expression, Stack<Expression> operandStack, String current) {
        return false;
    }
}
