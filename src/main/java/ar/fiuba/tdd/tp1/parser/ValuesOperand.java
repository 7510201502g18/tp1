package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.Stack;

/**
 * Created by Demian on 20/10/2015.
 */
public abstract class ValuesOperand extends ParenthesesMatchParser {
    ValuesOperand(Parser subParser) {
        super(subParser);
        medExp = "\\(.+(,.+)+\\)";
    }

    @Override
    protected boolean resolveInternalExpression(String expression, Stack<Expression> operandStack, String current) {
        String[] rangeSplit = expression.split(",");

        Value[] cellValueList = new Value[rangeSplit.length];
        for (int i = 0; i < rangeSplit.length; i++) {
            cellValueList[i] = subParser.parse(current, "= " + rangeSplit[i]);
        }

        buildExpression(operandStack, cellValueList);
        return true;
    }

    protected abstract void buildExpression(Stack<Expression> operandStack, Value[] valueList);
}
