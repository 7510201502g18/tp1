package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionCell;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Demian on 02/10/2015.
 */
public class BaseOperandCellParser implements ParserOperand {

    private static final String PAGE_EXPRESSION = "![a-z]*[A-Z]*[0-9]*+.";
    private static final String COLUMN_EXPRESSION = "[A-Z]+";
    private static final String ROW_EXPRESSION = "[0-9]+";

    private Document excelFiuba;

    public BaseOperandCellParser(Document excelFiuba) {
        this.excelFiuba = excelFiuba;
    }


    public boolean parse(String expression, Stack<Expression> operandStack, String actualSheet) {
        if (isACellReference(expression)) {

            Expression formula = new ExpressionCell(getCellFromExpression(expression));
            operandStack.push(formula);
            return true;
        } else if (isACellReferenceOfActualSheet(expression)) {
            Expression formula = new ExpressionCell(excelFiuba.getCell(actualSheet, expression));
            operandStack.push(formula);
            return true;
        }

        return false;
    }

    private boolean isACellReference(String expression) {
        String regularExpressionPattern = "^" + PAGE_EXPRESSION + COLUMN_EXPRESSION + ROW_EXPRESSION + "$";

        return expression.matches(regularExpressionPattern);
    }

    private boolean isACellReferenceOfActualSheet(String expression) {
        String regularExpressionPattern = "^" + COLUMN_EXPRESSION + ROW_EXPRESSION + "$";

        return expression.matches(regularExpressionPattern);
    }

    private String getMatchExpression(String expression, String regularExpression) {
        Pattern pattern = Pattern.compile(regularExpression);
        Matcher match = pattern.matcher(expression);

        if (match.find()) {
            return match.group(0);
        } else {
            //TODO:: lanzar alguna excepcion o algo
            System.out.println("Matcheo de: " + regularExpression + " no encontrado");
            return null;
        }
    }

    private String getPage(String expression) {
        String page = getMatchExpression(expression, PAGE_EXPRESSION);
        return page.substring(1, page.length() - 1);
    }

    private String getCell(String expression) {
//        return getMatchExpression(expression, "(" + COLUMN_EXPRESSION + ROW_EXPRESSION + ")", 2);
        String cell = getMatchExpression(expression, "\\." + COLUMN_EXPRESSION + ROW_EXPRESSION);
        return cell.substring(1, cell.length());
    }

    public Cell getCellFromExpression(String expression) {
//        String page = getPage(expression);
//        String cell = getCell(expression);
//
//        return excelFiuba.getCell(page, cell);
        String page = getPage(expression);

        String cell = getCell(expression);

        return excelFiuba.getCell(page, cell);
    }
}
