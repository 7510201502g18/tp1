package ar.fiuba.tdd.tp1.parser.resolver;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;

public class StringResolver extends TypeResolver {

    public StringResolver(TypeResolver nextResolver) {
        super(nextResolver);
    }

    @Override
    protected Expression getExpression(String expression) {
        return new ExpressionDefault(expression);
    }

    @Override
    protected boolean isThisType(String expression) {
        return patternFinded(".*", expression);
    }

}
