package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.List;
import java.util.Stack;

/**
 * Created by Demian on 20/10/2015.
 */
public abstract class RangeOperand extends ParenthesesMatchParser {
    Range range;

    public RangeOperand(Parser subParser) {
        super(subParser);
        medExp = "\\([A-Z]+[0-9]+:[A-Z]+[0-9]+\\)";
        range = new Range();
    }

    @Override
    protected boolean resolveInternalExpression(String expression, Stack<Expression> operandStack, String current) {
        String[] rangeSplit = expression.split(":");
        String topLeftCell = rangeSplit[0];
        String botRightCell = rangeSplit[1];

        List<String> cellList = range.buildRange(topLeftCell, botRightCell);
        Value[] cellValueList = new Value[cellList.size()];

        for (int i = 0; i < cellList.size(); i++) {
            Expression cellExpression = subParser.parse(current, "= " + cellList.get(i));
            cellValueList[i] = cellExpression;
        }

        this.buildExpression(cellValueList, operandStack);


        return true;

    }

    protected abstract void buildExpression(Value[] cellValueList, Stack<Expression> operandStack);
}
