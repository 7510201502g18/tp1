package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;

import java.util.Stack;

public class BaseOperandNumberParser implements ParserOperand {

    public boolean parse(String expression, Stack<Expression> operandStack, String current) {
        try {
            Expression newOperand = new ExpressionDefault(Double.parseDouble(expression));
            operandStack.push(newOperand);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
