package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.Cell;

/**
 * Created by Demian on 17/10/2015.
 */
public interface CellIterator {
    public boolean hasNext();

    public Cell next();
}
