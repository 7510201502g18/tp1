package ar.fiuba.tdd.tp1.parser.resolver;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class TypeResolver {
    TypeResolver nextResolver;
    Pattern pattern;
    Matcher match;

    public TypeResolver(TypeResolver nextResolver) {
        this.nextResolver = nextResolver;
    }

    protected Pattern compile(String pattern) {
        return Pattern.compile(pattern);
    }

    protected Matcher match(String string) {
        return pattern.matcher(string);
    }

    protected boolean patternFinded(String pattern, String string) {

        this.pattern = compile(pattern);
        match = match(string);

        return match.find();

    }

    protected abstract Expression getExpression(String expression);

    protected abstract boolean isThisType(String expression);

    public Expression resolve(String expression) {
//        System.out.println("entro typeResolver");
        if (isThisType(expression)) {
//            System.out.println("es de este tipo");
            return getExpression(expression);
        }
        return nextResolver.resolve(expression);
    }
}
