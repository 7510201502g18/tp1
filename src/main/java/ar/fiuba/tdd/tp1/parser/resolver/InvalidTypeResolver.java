package ar.fiuba.tdd.tp1.parser.resolver;

import ar.fiuba.tdd.tp1.extern.driver.BadFormulaException;
import ar.fiuba.tdd.tp1.model.expressions.Expression;

public class InvalidTypeResolver extends TypeResolver {

    public InvalidTypeResolver(TypeResolver nextResolver) {
        super(nextResolver);
    }

    @Override
    protected Expression getExpression(String expression) {
        throw new BadFormulaException();
    }

    @Override
    protected boolean isThisType(String expression) {
        return true;
    }


}
