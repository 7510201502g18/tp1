package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.Stack;

/**
 * Created by Demian on 16/10/2015.
 */
public class ParenthesesOperand extends ParenthesesMatchParser {


    ParenthesesOperand(Parser subParser) {
        super(subParser);
    }

    @Override
    protected boolean resolveInternalExpression(String expression, Stack<Expression> operandStack, String current) {

        Expression subExpression = subParser.parse(current, "= " + expression);
        operandStack.push(subExpression);
        return true;
    }
}
