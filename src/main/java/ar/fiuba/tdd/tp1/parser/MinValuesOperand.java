package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.nary.ExpressionMin;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.Stack;

/**
 * Created by Demian on 17/10/2015.
 */
public class MinValuesOperand extends ValuesOperand {
    MinValuesOperand(Parser subParser) {
        super(subParser);
        preExp = "MIN";
    }

    @Override
    protected void buildExpression(Stack<Expression> operandStack, Value[] valueList) {
        operandStack.push(new ExpressionMin(valueList));
    }
}
