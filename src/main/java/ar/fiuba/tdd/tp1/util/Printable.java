package ar.fiuba.tdd.tp1.util;

public interface Printable {

    void print(String str);

}
