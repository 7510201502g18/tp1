package ar.fiuba.tdd.tp1.util;

public class CellNode {
    int row;
    int col;

    CellNode(int row, int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return Helper.colAndRowToIndexString(col, row);
    }

}
