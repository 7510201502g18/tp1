package ar.fiuba.tdd.tp1.deserialize;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.persistance.Persister;

import com.opencsv.CSVReader;

import java.io.*;


public class Deserializer extends Persister {

    public Deserializer() {
        super();
    }

    public Document deserializeJson(String jsonFile) {
        try {
            Document document = gson.fromJson(new InputStreamReader(new FileInputStream(new File(jsonFile)), "UTF-8"), Document.class);
            return document;
        } catch (JsonSyntaxException | JsonIOException | FileNotFoundException | UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public void deserializeSheetInCSV(Document document, String nameFile) {
        try {
            this.readerCSV = new CSVReader(new InputStreamReader(new FileInputStream(new File(nameFile)), "UTF-8"));

            String[] nextLine;
            while ((nextLine = readerCSV.readNext()) != null) {
                document.setValueInCurrentSheet(nextLine[0], nextLine[1]);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
