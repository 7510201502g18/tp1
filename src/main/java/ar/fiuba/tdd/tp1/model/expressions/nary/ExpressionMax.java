package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionMax extends ExpressionNary {

    public ExpressionMax(Value... ops) {
        super(ops);
    }

    @Override
    public ValueHolder evaluate() {
        double max = ops[0].getValue().getNumerical();
        for (int i = 0; i < ops.length; i++) {
            if (ops[i].getValue().getNumerical() > max) {
                max = ops[i].getValue().getNumerical();
            }
        }

        return new ValueHolderImp(max);
    }

}
