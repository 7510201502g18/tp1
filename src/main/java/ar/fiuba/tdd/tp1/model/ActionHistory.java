package ar.fiuba.tdd.tp1.model;

import java.util.LinkedList;

/* Descripcion:
 * Maneja y administra las distintas acciones que se ejecuten sobre el documento*/

public class ActionHistory {
    private LinkedList<Action> actionsRedo;
    private LinkedList<Action> actionsUndo;

    public ActionHistory() {
        actionsRedo = new LinkedList<>();
        actionsUndo = new LinkedList<>();
    }

    public void setCellAction(Cell cell) {
        actionsUndo.add(new ChangeCellAction(cell));
        actionsRedo.clear();
    }

    public void undo() {
        if (!actionsUndo.isEmpty()) {
            Action action = actionsUndo.removeLast();
            actionsRedo.add(action);
            action.undo();
        }
    }

    public void redo() {
        if (!actionsRedo.isEmpty()) {
            Action action = actionsRedo.removeLast();
            actionsUndo.add(action);
            action.redo();
        }
    }

    public void setSpreedSheetAction(Document document, String spreedSheetName) {
        actionsUndo.add(new AddSpreedSheetAction(document, spreedSheetName));
    }
}
