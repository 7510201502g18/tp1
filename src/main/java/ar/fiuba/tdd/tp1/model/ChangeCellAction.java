package ar.fiuba.tdd.tp1.model;

public class ChangeCellAction implements Action {
    public Cell cell;

    public ChangeCellAction(Cell cell) {
        this.cell = cell;
    }

    @Override
    public void redo() {
        cell.redoExpression();
    }

    @Override
    public void undo() {
        cell.undoExpression();
    }
}
