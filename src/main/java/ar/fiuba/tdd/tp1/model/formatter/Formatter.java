package ar.fiuba.tdd.tp1.model.formatter;

public interface Formatter {
    String format();
}
