package ar.fiuba.tdd.tp1.model.expressions;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

import java.util.LinkedList;
import java.util.List;


public class ExpressionDefault implements Expression {

    private ValueHolder value;

    public ExpressionDefault(double number) {
        this.value = new ValueHolderImp(number);
    }

    public ExpressionDefault(String string) {
        this.value = new ValueHolderImp(string);
    }

    @Override
    public ValueHolder evaluate() {
        return value;
    }

    @Override
    public List<Cell> getCellsInvolve() {
        return new LinkedList<>();
    }

    @Override
    public ValueHolder getValue() {
        return value;
    }


}
