package ar.fiuba.tdd.tp1.model.expressions;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;

public interface Expression extends Value {

    ValueHolder evaluate();
}
