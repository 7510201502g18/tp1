package ar.fiuba.tdd.tp1.model.value;

import ar.fiuba.tdd.tp1.extern.driver.BadFormulaException;

import java.text.ParseException;

public interface ValueHolder {

    double getNumerical() throws BadFormulaException;

    String getLiteral() throws BadFormulaException;

    String getAsString();

    void setFormatter(String typeFormat, String formattter) throws ParseException;

    void setType(String type) throws ParseException;

}
