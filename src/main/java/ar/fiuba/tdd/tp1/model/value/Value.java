package ar.fiuba.tdd.tp1.model.value;

import ar.fiuba.tdd.tp1.model.Cell;

import java.util.List;

public interface Value {

    ValueHolder getValue();

    List<Cell> getCellsInvolve();
}