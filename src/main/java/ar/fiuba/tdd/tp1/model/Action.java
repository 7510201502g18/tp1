package ar.fiuba.tdd.tp1.model;

public interface Action {

    void redo();

    void undo();

}
