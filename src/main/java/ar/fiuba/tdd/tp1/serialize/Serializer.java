package ar.fiuba.tdd.tp1.serialize;

import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.model.SpreedSheet;
import ar.fiuba.tdd.tp1.persistance.Persister;
import ar.fiuba.tdd.tp1.util.Printer;

import java.io.*;

public class Serializer extends Persister {

    public Serializer() {
        super();
    }

    public void serializeInJson(Document document, String fileName) {

        try {

            File file = new File(fileName);
            Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
            writer.write(gson.toJson(document));
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void serializeSheetInCSV(SpreedSheet spreedSheet, String nameFile) {
        try {
            //this.writerCSV = new CSVWriter(new OutputStreamWriter(new FileOutputStream(new File(nameFile)), "UTF-8"));

            PrintWriter writer = new PrintWriter(nameFile, "UTF-8");
            Printer.getInstance().printCSV(spreedSheet, writer);

//            //Write version and project name
//            Collection<Cell> cells = spreedSheet.getCells();
//            for (Cell cell : cells) {
//                this.writerCSV.writeNext((cell.getCellName() + "," + cell.getValueAsString()).split(","));
//            }
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


}
